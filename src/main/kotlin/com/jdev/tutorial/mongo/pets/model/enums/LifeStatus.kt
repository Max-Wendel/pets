package com.jdev.tutorial.mongo.pets.model.enums

enum class LifeStatus {
    UNDEFINED,  // :|
    HEALTHY,    // :D
    GOOD,       // :)
    DISEASED,   // :(
    DEAD        // x(
}
