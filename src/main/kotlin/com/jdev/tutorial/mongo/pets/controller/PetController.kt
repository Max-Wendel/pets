package com.jdev.tutorial.mongo.pets.controller

import com.jdev.tutorial.mongo.pets.model.Pet
import com.jdev.tutorial.mongo.pets.model.dto.PetDTO
import com.jdev.tutorial.mongo.pets.repository.PetRepository
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/pets")
class PetController {

    @Autowired
    lateinit var repository: PetRepository

    @GetMapping("/")
    @ResponseBody
    fun listAll(): HttpEntity<*> {
        return HttpEntity(
                repository.findAll().map {
                    PetDTO(it)
                }
        )
    }

    @GetMapping("/get/")
    @ResponseBody
    fun getByID(
            @RequestParam(name = "id") id: ObjectId
    ): HttpEntity<*> {
        return HttpEntity(
                PetDTO(repository.findBy_id(id))
        )
    }

    @PutMapping("/update/")
    @ResponseBody
    fun update(
            @RequestParam(name = "id") id: String,
            @RequestBody pet: Pet
    ): HttpEntity<*> {
        pet._id = ObjectId(id)
        repository.save(pet)
        return HttpEntity(HttpStatus.OK)
    }

    @PostMapping("/save/")
    @ResponseBody
    fun create(
            @RequestBody pet: Pet
    ): HttpEntity<*> {
        repository.save(pet)
        return HttpEntity(
                PetDTO(repository.findBy_id(pet._id))
        )
    }

    @DeleteMapping("/delete")
    @ResponseBody
    fun delete(
            @RequestParam(name = "id") id: ObjectId
    ): HttpEntity<*> {
        repository.deleteBy_id(id)
        return HttpEntity(HttpStatus.OK)
    }

}