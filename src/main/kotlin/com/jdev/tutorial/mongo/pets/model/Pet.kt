package com.jdev.tutorial.mongo.pets.model

import com.jdev.tutorial.mongo.pets.model.enums.PetGender
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "pet")
data class Pet(
        @Id
        var _id: ObjectId = ObjectId.get(),
        var name: String = "",
        var species: String = "",
        var breed: String = "",
        var age: Int = 0,
        var gender: PetGender = PetGender.UNDEFINED
) {
    constructor(pet: Pet) : this(
            _id = pet._id,
            name = pet.name,
            species = pet.species,
            breed = pet.breed,
            age = pet.age,
            gender = pet.gender
    )
}