package com.jdev.tutorial.mongo.pets.model.dto

import com.jdev.tutorial.mongo.pets.model.Pet
import com.jdev.tutorial.mongo.pets.model.enums.PetGender

data class PetDTO(
        var id: String = "",
        var name: String = "",
        var species: String = "",
        var breed: String = "",
        var age: Int = 0,
        var gender: PetGender = PetGender.UNDEFINED
) {
    constructor(pet: Pet) : this(
            id = pet._id.toHexString(),
            name = pet.name,
            species = pet.species,
            breed = pet.breed,
            age = pet.age,
            gender = pet.gender
    )
}