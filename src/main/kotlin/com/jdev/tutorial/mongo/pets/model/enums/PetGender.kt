package com.jdev.tutorial.mongo.pets.model.enums

enum class PetGender {
    UNDEFINED,
    FEMALE,
    MALE
}
