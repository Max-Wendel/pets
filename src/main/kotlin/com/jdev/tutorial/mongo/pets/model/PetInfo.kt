package com.jdev.tutorial.mongo.pets.model

import com.jdev.tutorial.mongo.pets.model.enums.LifeStatus
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document(collection = "pet_info")
data class PetInfo(
        var _id: ObjectId = ObjectId.get(),
        var pet: Pet = Pet(),
        var petOwner: PetOwner = PetOwner(),
        var dateEntry: Date = _id.date,
        var lifeStatus: LifeStatus = LifeStatus.UNDEFINED
) {
}