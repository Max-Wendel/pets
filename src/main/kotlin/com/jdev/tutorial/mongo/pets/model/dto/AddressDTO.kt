package com.jdev.tutorial.mongo.pets.model.dto

import com.jdev.tutorial.mongo.pets.model.Address

data class AddressDTO(
        var street: String = "",
        var district: String = "",
        var country: String = "",
        var zip: String = ""
) {
    constructor(address: Address) : this(
            street = address.street,
            district = address.district,
            country = address.country,
            zip = address.zip
    )
}