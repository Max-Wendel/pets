package com.jdev.tutorial.mongo.pets.model.dto

import com.jdev.tutorial.mongo.pets.model.PetOwner
import com.jdev.tutorial.mongo.pets.model.enums.OwnerGender

data class PetOwnerDTO(
        var name: String = "",
        var age: Int = 0,
        var gender: OwnerGender = OwnerGender.UNDEFINED,
        var address: AddressDTO = AddressDTO()
) {
    constructor(petOwner: PetOwner) : this(
            name = petOwner.name,
            age = petOwner.age,
            gender = petOwner.gender,
            address = AddressDTO(petOwner.address)
    )
}