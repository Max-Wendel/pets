package com.jdev.tutorial.mongo.pets.model

import com.jdev.tutorial.mongo.pets.model.enums.OwnerGender
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "pet_owner")
data class PetOwner(
        var _id: ObjectId = ObjectId.get(),
        var name: String = "",
        var age: Int = 0,
        var gender: OwnerGender = OwnerGender.UNDEFINED,
        var address: Address = Address()
)
