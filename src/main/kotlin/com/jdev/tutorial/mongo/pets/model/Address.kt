package com.jdev.tutorial.mongo.pets.model

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "address")
data class Address(
        var _id: ObjectId = ObjectId.get(),
        var street: String = "",
        var district: String = "",
        var country: String = "",
        var zip: String = ""
)
