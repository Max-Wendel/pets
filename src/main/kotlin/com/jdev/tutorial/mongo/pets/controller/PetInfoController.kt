package com.jdev.tutorial.mongo.pets.controller

import com.jdev.tutorial.mongo.pets.model.PetInfo
import com.jdev.tutorial.mongo.pets.model.dto.PetInfoDTO
import com.jdev.tutorial.mongo.pets.repository.PetInfoRepository
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/infos")
class PetInfoController {

    @Autowired
    lateinit var repository: PetInfoRepository

    @GetMapping("/")
    @ResponseBody
    fun listAll(): HttpEntity<*> {
        return HttpEntity(repository.findAll().map {
            PetInfoDTO(it)
        })
    }

    @GetMapping("/get/")
    @ResponseBody
    fun getByID(
            @RequestParam(name = "id") id: ObjectId
    ): HttpEntity<*> {
        return HttpEntity(
                PetInfoDTO(repository.findBy_id(id))
        )
    }

    @PutMapping("/update/")
    @ResponseBody
    fun update(
            @RequestParam(name = "id") id: String,
            @RequestBody petInfo: PetInfo
    ): HttpEntity<*> {
        petInfo._id = ObjectId(id)
        repository.save(petInfo)
        return HttpEntity(HttpStatus.OK)
    }

    @PostMapping("/save/")
    @ResponseBody
    fun create(
            @RequestBody petInfo: PetInfo
    ): HttpEntity<*> {
        repository.save(petInfo)
        return HttpEntity(
                PetInfoDTO(repository.findBy_id(petInfo._id))
        )
    }

    @DeleteMapping("/delete")
    @ResponseBody
    fun delete(
            @RequestParam(name = "id") id: ObjectId
    ): HttpEntity<*> {
        repository.deleteBy_id(id)
        return HttpEntity(HttpStatus.OK)
    }
}