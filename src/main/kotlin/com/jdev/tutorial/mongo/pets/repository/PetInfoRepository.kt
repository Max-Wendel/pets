package com.jdev.tutorial.mongo.pets.repository

import com.jdev.tutorial.mongo.pets.model.PetInfo
import com.jdev.tutorial.mongo.pets.model.PetOwner
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface PetInfoRepository : MongoRepository<PetInfo, String> {

    fun findBy_id(_id: ObjectId): PetInfo

    fun findByPetOwner(petOwner: PetOwner): List<PetInfo>

    fun deleteBy_id(_id: ObjectId)
}