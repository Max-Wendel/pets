package com.jdev.tutorial.mongo.pets.controller

import com.jdev.tutorial.mongo.pets.model.PetOwner
import com.jdev.tutorial.mongo.pets.model.dto.PetOwnerDTO
import com.jdev.tutorial.mongo.pets.repository.PetOwnerRepository
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/owners")
class PetOwnerController {

    @Autowired
    lateinit var repository: PetOwnerRepository

    @GetMapping("/")
    @ResponseBody
    fun listAll(): HttpEntity<*> {
        return HttpEntity(repository.findAll())
    }

    @GetMapping("/get/")
    @ResponseBody
    fun getByID(
            @RequestParam(name = "id") id: ObjectId
    ): HttpEntity<*> {
        return HttpEntity(
                PetOwnerDTO(repository.findBy_id(id))
        )
    }

    @PutMapping("/update/")
    @ResponseBody
    fun update(
            @RequestParam(name = "id") id: String,
            @RequestBody petOwner: PetOwner
    ): HttpEntity<*> {
        petOwner._id = ObjectId(id)
        repository.save(petOwner)
        return HttpEntity(HttpStatus.OK)
    }

    @PostMapping("/save/")
    @ResponseBody
    fun create(
            @RequestBody petOwner: PetOwner
    ): HttpEntity<*> {
        repository.save(petOwner)
        return HttpEntity(
                PetOwnerDTO(repository.findBy_id(petOwner._id))
        )
    }

    @DeleteMapping("/delete")
    @ResponseBody
    fun delete(
            @RequestParam(name = "id") id: ObjectId
    ): HttpEntity<*> {
        repository.deleteBy_id(id)
        return HttpEntity(HttpStatus.OK)
    }
}