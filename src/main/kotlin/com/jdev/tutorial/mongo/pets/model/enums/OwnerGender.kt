package com.jdev.tutorial.mongo.pets.model.enums

enum class OwnerGender {
    UNDEFINED,
    MEN,
    WOMAN,
    OTHER
}
