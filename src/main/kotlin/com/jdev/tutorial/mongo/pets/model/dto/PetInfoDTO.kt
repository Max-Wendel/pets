package com.jdev.tutorial.mongo.pets.model.dto

import com.jdev.tutorial.mongo.pets.model.PetInfo
import com.jdev.tutorial.mongo.pets.model.enums.LifeStatus

data class PetInfoDTO(
        var pet: PetDTO = PetDTO(),
        var petOwner: PetOwnerDTO = PetOwnerDTO(),
        var lifeStatus: LifeStatus = LifeStatus.UNDEFINED
) {
    constructor(petInfo: PetInfo) : this(
            pet = PetDTO(petInfo.pet),
            petOwner = PetOwnerDTO(petInfo.petOwner),
            lifeStatus = petInfo.lifeStatus
    )
}