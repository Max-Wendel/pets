package com.jdev.tutorial.mongo.pets.repository

import com.jdev.tutorial.mongo.pets.model.PetOwner
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface PetOwnerRepository : MongoRepository<PetOwner, String> {

    fun findBy_id(_id: ObjectId): PetOwner

    fun findByName(name: String): List<PetOwner>

    fun deleteBy_id(_id: ObjectId)

}